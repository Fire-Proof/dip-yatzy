var Dice = function () {
    this.val = 0;

    this.roll = function () {
        this.val = Math.floor(Math.random() * 6) + 1
    };
};

var YatzyDice = function () {
    this.die = [new Dice(), new Dice(), new Dice(), new Dice(), new Dice()];

    this.roll = function (hold) {
        for (var i = 0; i < this.die.length; i++) {
            if (!hold[i]) {
                this.die[i].roll();
            }
        }
    };

    this.frequency = function () {
        var array = {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0
        };
        for (var i = 0; i < this.die.length; i++) {
            array[this.die[i].val]++;
        }
        return array
    };

    this.sameValuePoints = function (value) {
        return this.frequency()[value] * value
    };

    this.onePairPoints = function () {
        var freq = this.frequency();
        for (var i = 6; i > 0; i--) {
            if (freq[i] >= 2) {
                return i * 2;
            }
        }
        return 0;
    };

    this.twoPairPoints = function () {
        var pair_one = 0;

        var freq = this.frequency();
        for (var i = 6; i > 0; i--) {
            if (freq[i] >= 2) {
                if (pair_one === 0) {
                    pair_one = i * 2;
                } else {
                    return i * 2 + pair_one;
                }
            }
        }
        return 0;
    };

    this.threeSamePoints = function () {
        var freq = this.frequency();
        for (var i = 6; i > 0; i--) {
            if (freq[i] >= 3) {
                return i * 3;
            }
        }
        return 0;
    };

    this.fourSamePoints = function () {
        var freq = this.frequency();
        for (var i = 6; i > 0; i--) {
            if (freq[i] >= 4) {
                return i * 4;
            }
        }
        return 0;
    };

    this.fullHousePoints = function () {
        var freq = this.frequency();
        var first_part = 0;

        for (var i = 6; i > 0; i--) {
            if (freq[i] === 3) {
                first_part = i * 3;
            }
        }

        if (first_part !== 0) {
            for (var j = 6; j > 0; j--) {
                if (freq[j] === 2) {
                    return j * 2 + first_part;
                }
            }
        }

        return 0;
    };

    this.smallStraightPoints = function () {
        var array = {
            1: 1,
            2: 1,
            3: 1,
            4: 1,
            5: 1,
            6: 0
        };

        var freq = this.frequency();

        for (var i = 6; i > 0; i--) {
            if (freq[i] !== array[i]) {
                return 0;
            }
        }
        return 15;
    };

    this.largeStraightPoints = function () {
        var array = {
            1: 0,
            2: 1,
            3: 1,
            4: 1,
            5: 1,
            6: 1
        };

        var freq = this.frequency();
        for (var i = 6; i > 0; i--) {
            if (freq[i] !== array[i]) {
                return 0;
            }
        }
        return 20;
    };

    this.chancePoints = function () {
        var sum = 0;
        this.die.forEach(function (t) {
            sum += t.val
        });
        return sum
    };

    this.yatzyPoints = function () {
        var freq = this.frequency();
        for (var i = 6; i > 0; i--) {
            if (freq[i] === 5) {
                return i * 5;
            }
        }
        return 0;
    };


    this.returnAll = function () {
        return {
            '1s': this.sameValuePoints(1),
            '2s': this.sameValuePoints(2),
            '3s': this.sameValuePoints(3),
            '4s': this.sameValuePoints(4),
            '5s': this.sameValuePoints(5),
            '6s': this.sameValuePoints(6),
            'onePair': this.onePairPoints(),
            'twoPair': this.twoPairPoints(),
            'threeSame': this.threeSamePoints(),
            'fourSame': this.fourSamePoints(),
            'fullHouse': this.fullHousePoints(),
            'smallStraight': this.smallStraightPoints(),
            'largeStraight': this.largeStraightPoints(),
            'chance': this.chancePoints(),
            'yatzy': this.yatzyPoints()
        }
    }

};

var Yatzy = function () {
    this.yatzy_dice = new YatzyDice();
    this.roll_counter = 0;

    this.score = {
        '1s': null,
        '2s': null,
        '3s': null,
        '4s': null,
        '5s': null,
        '6s': null,
        'onePair': null,
        'twoPair': null,
        'threeSame': null,
        'fourSame': null,
        'fullHouse': null,
        'smallStraight': null,
        'largeStraight': null,
        'chance': null,
        'yatzy': null
    };

    this.roll = function (hold) {
        if (this.roll_counter <= 2) {
            this.yatzy_dice.roll(hold);
            this.roll_counter++
        }
    };

    this.lockScore = function (value) {
        if (this.score[value] === null) {
            this.score[value] = this.yatzy_dice.returnAll()[value];
        }
    };

    this.firstSum = function () {
        return this.score['1s'] + this.score['2s']
            + this.score['3s'] + this.score['4s']
            + this.score['5s'] + this.score['6s']
    };

    this.bonus = function () {
        if (this.firstSum() >= 63) {
            return 50;
        }
        return 0;
    };

    this.secondSum = function () {
        return this.score['onePair'] + this.score['twoPair']
            + this.score['threeSame'] + this.score['fourSame']
            + this.score['fullHouse'] + this.score['smallStraight']
            + this.score['largeStraight'] + this.score['chance']
            + this.score['yatzy']
    };

    this.sum = function () {
        return this.firstSum() + this.bonus() + this.secondSum()
    };

    this.resetRoll = function () {
        this.yatzy_dice = new YatzyDice();
        this.roll_counter = 0
    };

    this.resetScore = function () {
        this.score = {
            '1s': null,
            '2s': null,
            '3s': null,
            '4s': null,
            '5s': null,
            '6s': null,
            'onePair': null,
            'twoPair': null,
            'threeSame': null,
            'fourSame': null,
            'fullHouse': null,
            'smallStraight': null,
            'largeStraight': null,
            'chance': null,
            'yatzy': null
        };
    }

};

var GUI = function () {
    var self = this;
    this.yatzy = new Yatzy();

    var dice_icons = {
        1: "⚀",
        2: "⚁",
        3: "⚂",
        4: "⚃",
        5: "⚄",
        6: "⚅",
        0: "□"
    };


    this.registerButtons = function () {
        $('#gameOverButton').click(this.restartGame);
        $('#roll').click(this.roll);
    };

    this.roll = function () {
        self.setHoldDisabled(self.yatzy.roll_counter >= 2);
        if (self.yatzy.roll_counter !== 3) {
            self.yatzy.roll(self.getHold());
            $('#currentRoll').text(self.yatzy.roll_counter);
            self.updateDice();
            self.updateScore()
        }
    };

    this.animateDice = function (id, final) {
        var sel = $("#dice-" + id).find(".number");
        if (final === 0) { // Don't animate if 0
            sel.text(dice_icons[final])
        } else {
            var changeToRandom = function (sel) {
                sel.text(dice_icons[Math.floor(Math.random() * 6) + 1])
            };
            if (!self.getHold()[id - 1]) {  // animate roll if not held
                for (var i = 0; i < 5; i++) {
                    setTimeout(changeToRandom, 100 * i, sel)
                }
                setTimeout(function (sel) {
                    sel.text(dice_icons[final])
                }, 600, sel)
            }
        }
    };

    this.restartGame = function () {
        self.yatzy.resetScore();
        self.yatzy.resetRoll();
        self.updateDice();
        self.updateScore();
        self.setHoldDisabled(true);
        self.setHold(false);
        $("#gameOver").addClass("hidden");
    };

    this.updateDice = function () {
        var die = this.yatzy.yatzy_dice.die;
        self.animateDice(1, die[0].val);
        self.animateDice(2, die[1].val);
        self.animateDice(3, die[2].val);
        self.animateDice(4, die[3].val);
        self.animateDice(5, die[4].val);
    };

    this.updateScore = function () {
        var val = this.yatzy.yatzy_dice.returnAll();
        var score = this.yatzy.score;
        for (var index in score) {
            var sel = $('#' + index).find(".value");
            sel.unbind('click');
            if (score[index] !== null) {
                sel.text(score[index]);
                sel.addClass('locked')
            } else {
                sel.text(val[index]);
                sel.removeClass('locked');
                if (this.yatzy.roll_counter !== 0) {
                    sel.click(this.scoreClicked)
                }
            }
        }

        $('#firstSum').text(this.yatzy.firstSum());
        $('#bonusSum').text(this.yatzy.bonus());
        $('#secondSum').text(this.yatzy.secondSum());
        $('#totalSum').text(this.yatzy.sum());

    };

    this.scoreClicked = function (val) {
        var id = $(val.target.parentNode).attr('id');
        self.yatzy.lockScore(id);

        var game_over = true;
        for (var index in self.yatzy.score) {
            if (self.yatzy.score[index] === null) {
                game_over = false
            }
        }

        if (game_over) {
            self.updateScore();
            var sel = $("#gameOver");
            sel.removeClass("hidden");
            sel.find(".score").text(self.yatzy.sum());
            self.yatzy.resetScore()

        } else {
            self.yatzy.resetRoll();
            $('#currentRoll').text(self.yatzy.roll_counter);
            self.setHoldDisabled(true);
            self.setHold(false);
            self.updateDice();
            self.updateScore();
        }
    };

    this.getHold = function () {
        return [
            $('#dice-1').find("input").prop('checked'),
            $('#dice-2').find("input").prop('checked'),
            $('#dice-3').find("input").prop('checked'),
            $('#dice-4').find("input").prop('checked'),
            $('#dice-5').find("input").prop('checked')
        ]
    };

    this.setHoldDisabled = function (bool) {
        $('#dice-1').find("input").prop('disabled', bool);
        $('#dice-2').find("input").prop('disabled', bool);
        $('#dice-3').find("input").prop('disabled', bool);
        $('#dice-4').find("input").prop('disabled', bool);
        $('#dice-5').find("input").prop('disabled', bool)
    };

    this.setHold = function (bool) {
        $('#dice-1').find("input").prop('checked', bool);
        $('#dice-2').find("input").prop('checked', bool);
        $('#dice-3').find("input").prop('checked', bool);
        $('#dice-4').find("input").prop('checked', bool);
        $('#dice-5').find("input").prop('checked', bool)
    }

};

$(document).ready(function () {
    var gui = new GUI();
    gui.registerButtons();
    gui.updateDice();
});